package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo) {
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setWindSpeed((int) weatherInfo.wind.speed);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setLastUpdate(weatherInfo.dt);
    }
}
