package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {
    public Main main;
    public Wind wind;
    public Clouds clouds;
    public List<Weather> weather;
    public long dt;

    public static class Main {
        public float temp;
        public int humidity;
    }

    public static class Wind {
        public float speed;
        public float deg;
    }

    public static class Clouds {
        public int all;
    }

    public static class Weather {
        public String icon;
        public String description;
    }
}
